# New Site (Drupal 9)

## What is this?

This project is part of the
[Boston Drupam Meetup](https://www.meetup.com/Boston-Drupal-Group/)
event for 2021-03-09,
[New LAMPs for Old: Drupal well by Drupaling good](https://www.meetup.com/Boston-Drupal-Group/events/qqhbdsyccfbmb/).

Use this project to set up a
[Drupal](https://www.drupal.org/)
site, using the current version (Drupal 9).

There is a companion repository,
[Legacy Site](https://gitlab.com/boston-drupal-meetup/legacy),
which is only visible to volunteers working on this project.
Use that to set up a copy of the existing (legacy) Drupal site.

The goal is to upgrade the legacy site to Drupal 9 by migrating its content to
this site.

## More information

There is a version of the following instructions, and perhaps more, on the
[project wiki](https://gitlab.com/boston-drupal-meetup/newsite/-/wikis/home).
Be bold! Edit the wiki with questions, answers, corrections.

## Prerequisites

### Lando for Local Development

#### Get Lando

1. [https://lando.dev/download/](https://lando.dev/download/)
1. Follow the [download](https://github.com/lando/lando/releases) link.
1. Follow instructions.

#### Why Lando

- You can run your Drupal site from your own computer.
- There are other options. I like Lando, and this project uses it.
- Give it a try. At least start the download.
- More to come: [Wicked Local Development](#user-content-wicked-local-development)

### GitLab for Project Hosting

#### Get a GitLab account (free)

If you want to participate in this project, get an account.

1. [https://about.gitlab.com/pricing/](https://about.gitlab.com/pricing/)
1. FREE box: "Start now" button
1. Give me your user name on the `#boston` channel of
[Drupal Slack](https://www.drupal.org/community/contributor-guide/reference-information/talk/tools/slack)
or Zoom chat.
1. Get an invitation to the project.
1. Optional: set up
   [SSH keys on GitLab](https://docs.gitlab.com/ee/ssh/README.html).

#### Why GitLab

- Issues
- Wiki
- Code repository (git)
- Time tracking
- Much more

# Wicked Local Development

## Get the files

1. Make a directory, say `boston-drupal-meetup/`.
1. Open a terminal in `boston-drupal-meetup`.
1. Clone the project with one of the following:
   1. With SSH keys: `git clone git@gitlab.com:boston-drupal-meetup/newsite.git`
   1. Without SSH keys: `git clone https://gitlab.com/boston-drupal-meetup/newsite.git`
   1. Without terminal/`git`: use the Download button next to the blue Clone button
   on the [project page](https://gitlab.com/boston-drupal-meetup/newsite).

## Start your LAMP stack

1. In terminal, go to `boston-drupal-meetup/newsite/`.
1. `lando start`
1. `lando composer install`
1. `lando drush site:install --db-url=mysql://drupal9:drupal9@database/drupal9 --existing-config`
1. `lando drush user:login`

If you get the error message

> Existing configuration directory not found or does not contain a core.extension.yml file.".

after the `site:install` step, then try again. I should look into that ...

If all goes well, then the `user:login` step should give you a link you can use
to log in to your local copy of the site as the site administrator, something
like
[https://newsite.lndo.site/user/reset/1/1615058762/ir0JZEYNnRHHiWO7dbGnG5gjIsMZK48HEpwcxK-O-9w/login](https://newsite.lndo.site/user).
Your browser should warn you about using HTTPS with an untrusted host.
You can either use the HTTP version of the link or tell your browser not to
worry.

## Do it again!

[Follow similar steps](https://gitlab.com/boston-drupal-meetup/legacy/-/blob/main/README.md#quick-start) to set up a local copy of the legacy site.
