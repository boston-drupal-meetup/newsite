## What?

<!-- Along with the title, this section gives a brief description of the issue.
-->

## Why?

<!-- What is the motivation for this issue? How does it fit into the project?
-->

## Who?

<!-- You probably think in terms of roles, like Project Manager and Developer,
but try to give functional requirements. "You need admin access to a copy of the
legacy site" or "You need to know how to create a merge request" are good
examples.

You may also suggest that this is a large issue that several people can work on,
or that it is a small task, good for someone who wants a quick win. -->

## How?

<!-- Suggest some steps for completing the task. If possible, give links or
provide code snippets that can be copied and pasted. -->

## When?

<!-- It is OK to leave this empty. If the issue should be done near the
beginning of the project or near the end, then say so. -->
