## Track your time

When you create this issue and assign it to yourself, you will get a timestamp.
Use that to figure out how much time you spend working.
When you go away and come back, add a comment to set a new timestamp.
You can log your time with a
[quick action](https://docs.gitlab.com/ee/user/project/quick_actions.html):
for example, enter a comment like this:

```
/spend 1h 30m
```

## Checklist

Check off these items as you complete them. The first is a gimme!

- [ ] Create a GitLab account and get added to the project.
- [ ] Download and install Lando
- [ ] Download the code for this repo and for the [legacy site](https://gitlab.com/boston-drupal-meetup/legacy).
- [ ] Start up both sites using Lando.
- [ ] Log on to the new site as an admin. (Hint: `lando drush user:login`.)
- [ ] Go to Administration > Configuration > Development > Upgrade on the new
  site: [Upgrade](https://newsite.lndo.site/upgrade). Read the instructions and
  Continue.
- [ ] Select "migration" under "Source connection" and submit the form.
- [ ] Read the stern warning and continue.
- [ ] Have a look at the bad news/good news page. (Good news is collapsed by
  default.) Submit the form.
- [ ] Poke around at the site. See what works, what does not.
- [ ] Review the wiki. Can you improve the instructions for the next one who
  tries this?
- [ ] Make sure to log your time on this issue!

As we go through this project, we will start over and repeat these steps several
times. Eventually, the site owner will do them once with the real site.

## Resources

- The [project home page](https://gitlab.com/boston-drupal-meetup/newsite)
  has instructions for getting started.
- The [wiki](https://gitlab.com/boston-drupal-meetup/newsite/-/wikis/home)
  has the same instructions on the
  [Prerequisites](https://gitlab.com/boston-drupal-meetup/newsite/-/wikis/Prerequisites)
  and
  [Wicked Local Development](https://gitlab.com/boston-drupal-meetup/newsite/-/wikis/Wicked-Local-Development)
  pages. Maybe new, improved versions!
- Check the
  [Troubleshooting](https://gitlab.com/boston-drupal-meetup/newsite/-/wikis/Troubleshooting)
  page on the wiki. If your problem is not described there, then add a Q/A
  section. Once your problem is solved, add the answer!
- Add the "need help" label to this issue. If that label does not exist, then
  someone needs to work on
  [Issue 6](https://gitlab.com/boston-drupal-meetup/newsite/-/issues/6).
- Ask for help on the `#boston` channel in the
  [Drupal Slack workspace](https://www.drupal.org/community/contributor-guide/reference-information/talk/tools/slack).
