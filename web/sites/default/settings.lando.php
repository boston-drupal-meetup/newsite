<?php

if (getenv('LANDO_INFO') == FALSE) {
  return;
}

$lando_info = json_decode(getenv('LANDO_INFO'), TRUE);

$databases['default']['default'] = [
  'database' => $lando_info['database']['creds']['database'],
  'username' => $lando_info['database']['creds']['user'],
  'password' => $lando_info['database']['creds']['password'],
  'prefix' => '',
  'host' => $lando_info['database']['internal_connection']['host'],
  'port' => $lando_info['database']['internal_connection']['port'],
  'namespace' => 'Drupal\\Core\\Database\\Driver\\mysql',
  'driver' => 'mysql',
];

// Database for the migration source.
$databases['migration']['default'] = [
  'database' => 'drupal7',
  'username' => 'drupal7',
  'password' => 'drupal7',
  'prefix' => '',
  'host' => 'database.legacy.internal',
  'port' => '3306',
  'namespace' => 'Drupal\\Core\\Database\\Driver\\mysql',
  'driver' => 'mysql',
];
// @see https://www.drupal.org/project/drupal/issues/3096101.
$settings['source_connection'] = 'migration';
$settings['migrate_file_public_path'] = 'http://appserver.legacy.internal';

$settings['trusted_host_patterns'] = ['\.lndo\.site$'];

/**
 * Enable local development services.
 */
$settings['container_yamls'][] = DRUPAL_ROOT . '/sites/development.services.yml';

/**
 * Enable access to rebuild.php.
 */
$settings['rebuild_access'] = TRUE;

/**
 * Skip file system permissions hardening.
 */
$settings['skip_permissions_hardening'] = TRUE;
